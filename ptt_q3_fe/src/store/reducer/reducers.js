import * as act from "../actions/actions";

const initialState = {
  dataFood: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case act.insertFood_ACTION:
      return {
        ...state,
        dataFood: [...state.dataFood, action.payload],
      };
    case act.deleteFood_ACTION:
      return {
        ...state,
        dataFood: action.payload.dataDelete,
      };
      case act.eidtFood_ACTION:
        return Object.assign({}, state, {
          dataFood: state.dataFood.filter((item) => {
               return item.id !== action.id; //delete matched data
           }).concat(action.payload)
        }); 

    default:
      return state;
  }
};
