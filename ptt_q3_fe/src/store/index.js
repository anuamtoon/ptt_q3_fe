import { combineReducers } from 'redux';
import main from './reducer/reducers';

export default combineReducers({
  main,
});