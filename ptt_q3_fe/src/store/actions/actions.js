
  export const insertFood_ACTION = `insertFood_ACTION`;
  export const insertFood = (data) => {
    return async (dispatch) => {
        dispatch({
          type: insertFood_ACTION,
          payload: data,
        });
    };
  };


  export const deleteFood_ACTION = `deleteFood_ACTION`;
  export const deleteFood = (dataDelete) => {
    return async (dispatch) => {
        dispatch({
          type: deleteFood_ACTION,
          payload: dataDelete,
        });
    };
  };

  export const eidtFood_ACTION = `eidtFood_ACTION`;
  export const editFood = (data) => {
    return async (dispatch) => {
        dispatch({
          type: eidtFood_ACTION,
          payload: data,
        });
    };
  };