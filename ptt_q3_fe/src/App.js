import * as React from 'react';
import Grid from '@mui/material/Grid';
import MainScreen from './Components/MainScreen'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import ReduxLogger from 'redux-logger';
import { apiMiddleware } from 'redux-api-middleware';
import rootReducer from './store';
function App() {
  const store = createStore(
    rootReducer,
    {},
    applyMiddleware(...[ReduxThunk, ReduxLogger, apiMiddleware])
  );
  return (
    <Grid container spacing={2}>
      <Provider store={store}>
        <MainScreen />
      </Provider>
    </Grid>
  );
}

export default App;
