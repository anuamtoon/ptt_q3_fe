// imports the React Javascript Library
import React from "react";
//Card
import Stack from '@mui/material/Stack';
import Button from "@material-ui/core/Button";
import CardMedia from '@mui/material/CardMedia';

//Tabs
import { withStyles } from "@material-ui/core/styles";


const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 500,
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-end"
    },
    input: {
        display: "none"
    },
    searchRoot: {
        padding: "2px 4px",
        display: "flex",
        alignItems: "center",
        width: 400
    },
    searchInput: {
        marginLeft: 8,
        flex: 1
    },
    searchIconButton: {
        padding: 10
    },
    searchDivider: {
        width: 1,
        height: 28,
        margin: 4
    }
});

class ImageUploadCard extends React.Component {
    state = {
        mainState: "initial", // initial, search, gallery, uploaded
        imageUploaded: 0,
        selectedFile: null
    };

    handleUploadClick = event => {
        console.log(event.target.files[0]);
        var file = event.target.files[0];
        const reader = new FileReader();
        var url = reader.readAsDataURL(file);

        reader.onloadend = function (e) {
            this.setState({
                selectedFile: [reader.result]
            });
        }.bind(this);
        console.log(url); // Would see a path?

       
        this.setState({
            mainState: "uploaded",
            selectedFile: event.target.files[0],
            imageUploaded: 1
        });

        this.props.setLinkPicture(event.target.files[0].name)
    };

    renderInitialState() {
        const { classes, theme } = this.props;

        return (
            <Stack style={{ marginTop: 20, marginLeft: 170 }} direction="row" alignItems="center" spacing={2}>
                <label htmlFor="contained-button-file">
                    <input
                        accept="image/*"
                        className={classes.input}
                        id="contained-button-file"
                        multiple
                        type="file"
                        onChange={this.handleUploadClick}
                    />
                    <Button style={{ marginLeft: 20 }} variant="contained" component="span" color="primary">
                        Upload
                        </Button>
                    </label>
                    {
                        this.state.mainState == "uploaded" &&
                        <CardMedia
                            component="img"
                            height="140"
                            image={this.state.selectedFile}
                            sx={{width: 240}}
                        />
                    }
            </Stack>
        );
    }
    handleImageSearch(url) {
        var filename = url.substring(url.lastIndexOf("/") + 1);
        console.log(filename);
        this.setState({
            mainState: "uploaded",
            imageUploaded: true,
            selectedFile: url,
            fileReader: undefined,
            filename: filename
        });
    }

    handleSeachClose = event => {
        this.setState({
            mainState: "initial"
        });
    };

    imageResetHandler = event => {
        console.log("Click!");
        this.setState({
            mainState: "initial",
            selectedFile: null,
            imageUploaded: 0
        });
    };

    render() {
        return (
            <React.Fragment>
                {(this.renderInitialState())}
            </React.Fragment>
        );
    }
}

export default withStyles(styles, { withTheme: true })(ImageUploadCard);