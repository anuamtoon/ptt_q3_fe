import React from 'react'
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import CardMedia from '@mui/material/CardMedia';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import InputUnstyled from '@mui/base/InputUnstyled';
import { styled } from '@mui/system';
import { useSelector, useDispatch } from 'react-redux';
import * as mainAction from "../store/actions/actions";
import DeleteRoundedIcon from '@mui/icons-material/DeleteRounded';
import { pink, yellow } from '@mui/material/colors';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import EditIcon from '@mui/icons-material/Edit';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import ImageUpload from "./ImageUpload";
const blue = {
    100: '#DAECFF',
    200: '#80BFFF',
    400: '#3399FF',
    600: '#0072E5',
};

const grey = {
    50: '#F3F6F9',
    100: '#E7EBF0',
    200: '#E0E3E7',
    300: '#CDD2D7',
    400: '#B2BAC2',
    500: '#A0AAB4',
    600: '#6F7E8C',
    700: '#3E5060',
    800: '#2D3843',
    900: '#1A2027',
};

const Input = styled('input')({
    display: 'none',
});

const StyledInputElement = styled('input')(
    ({ theme }) => `
    width: 320px;
    font-size: 0.875rem;
    font-family: IBM Plex Sans, sans-serif;
    font-weight: 400;
    line-height: 1.5;
    color: ${theme.palette.mode === 'dark' ? grey[200] : grey[900]};
    background: ${theme.palette.mode === 'dark' ? grey[200] : grey[50]};
    border: 1px solid ${theme.palette.mode === 'dark' ? grey[200] : grey[300]};
    border-radius: 8px;
    padding: 12px 12px;
  
    &:hover {
      background: ${theme.palette.mode === 'dark' ? '' : grey[100]};
      border-color: ${theme.palette.mode === 'dark' ? grey[200] : grey[400]};
    }
  
    &:focus {
      outline: 3px solid ${theme.palette.mode === 'dark' ? blue[600] : blue[100]};
    }
  `,
);

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '50%',
    height: '50%',
    bgcolor: 'background.paper',
    boxShadow: 24,
    borderRadius: 3,
    p: 4,
};

const CustomInput = React.forwardRef(function CustomInput(props, ref) {
    return (
        <InputUnstyled components={{ Input: StyledInputElement }} {...props} ref={ref} />
    );
});

export default function MainScreen() {
    const { dataFood } = useSelector((state) => state.main);
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [nameFood, setNameFood] = React.useState('');
    const [linkPicture, setLinkPicture] = React.useState('');
    const [dataResponse, setDataResponse] = React.useState('');
    const [indexPick, setIndexPick] = React.useState(0);
    const [search, setSearch] = React.useState('');
    const [mode, setMode] = React.useState('create');
    const handleOpen = () => {
        setOpen(true)
        setMode('create')
    };
    const handleEdit = (index) => {
        setOpen(true)
        setMode('edit')
        setIndexPick(index)
        dataResponse.forEach((res, i) => {
            if (i === index) {
                setNameFood(res.data.name)
                setLinkPicture(res.data.picture)
            }
        });
    };
    const handleConfirmEdit = (index) => {
        dataResponse.forEach((res, i) => {
            if (i === index) {
                const data = {
                    name: nameFood,
                    picture: linkPicture
                }
                dispatch(
                    mainAction.editFood({
                        data
                    })
                )
            }
        });

        setOpen(false)
    }
    const handleDelete = (index) => {
        const dataDelete = dataFood.filter((_, i) => index !== i)
        dispatch(
            mainAction.deleteFood({
                dataDelete,
            })
        )
    };
    const handleClose = () => setOpen(false);
    const handleAddFood = () => {
        const data = {
            name: nameFood,
            picture: linkPicture
        }
        dispatch(
            mainAction.insertFood({
                data,
            })
        )
        setOpen(false)
    };


    const handleUploadClick = (event) => {
        console.log(event.target.files[0]);
        var file = event.target.files[0];
        const reader = new FileReader();
        var url = reader.readAsDataURL(file);

        reader.onloadend = function (e) {
            setLinkPicture({
                selectedFile: [reader.result]
            });
        }.bind(this);
        console.log(url); // Would see a path?

        setLinkPicture({
            mainState: "uploaded",
            selectedFile: event.target.files[0],
            imageUploaded: 1
        });
    };

    React.useEffect(() => {
        if (open && mode === "create") {
            setNameFood('')
            setLinkPicture('')
        }
    }, [open])

    React.useEffect(() => {
        setDataResponse(dataFood)
    }, [dataFood])

    return (
        <Grid container>
            <Grid style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', margin: '16px' }} alignItems='center' item xs={16}>
                <Typography marginLeft="8px" variant="h5" gutterBottom component="div">
                    ผลไม้
            </Typography>
                <Button onClick={handleOpen} variant="contained" style={{backgroundColor:'green'}} >เพิ่มผลไม้</Button>
            </Grid>
            <Grid item xs={16} style={{ display: 'flex', justifyContent: 'flex-end', marginRight: '16px' }}>
                <Stack spacing={2} style={{ width: '220px', padding: '8px' }}>
                    <Autocomplete
                        id="free-solo-demo"
                        freeSolo
                        options={dataFood.map((option) => option.data.name)}
                        renderInput={(params) => <TextField {...params} onChange={(e) => setSearch(e.target.value)} onBlur={(e) => setSearch(e.target.value)} label="Search here" />}
                    />
                </Stack>
            </Grid>
            <TableContainer style={{ marginTop: '20px', margin: '16px' }} component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>

                            <TableCell><Typography fontSize="18px" font-weight="500" >ชื่อผลไม้</Typography></TableCell>
                            <TableCell align="right"></TableCell>
                            <TableCell align="right"></TableCell>
                            <TableCell align="right"></TableCell>
                            <TableCell align="right"></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(dataResponse && dataResponse.length > 0) ? dataResponse.filter((val) => {
                            if (search === "") {
                                return val
                            } else if (val.data.name.toLowerCase().includes(search.toLowerCase())) {
                                return val
                            }
                        }).map((val, index) => (
                            <TableRow
                                key={val.data.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {val.data.name}
                                </TableCell>
                                <TableCell align="right"> <CardMedia
                                    component="img"
                                    height="140"
                                    image={val.data.picture}
                                /></TableCell>
                                <TableCell align="right"> </TableCell>
                                <TableCell align="right">
                                    <Tooltip title="Edit">
                                        <IconButton onClick={() => handleEdit(index)}>
                                            <EditIcon sx={{ color: yellow[900] }} />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <IconButton onClick={() => handleDelete(index)}>
                                            <DeleteRoundedIcon sx={{ color: pink[500] }} />
                                        </IconButton>
                                    </Tooltip>

                                </TableCell>
                                <TableCell align="right"></TableCell>
                            </TableRow>
                        )) : (<TableRow style={{ alignItems: 'center', justifyContent: 'center', }}>
                            <TableCell align="right">
                                <Typography paddingLeft='40px' variant="h7" component="div">
                                    ไม่มีรายการ
                            </Typography>
                            </TableCell>
                        </TableRow>)}
                    </TableBody>
                </Table>
            </TableContainer>
            <Modal
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {mode === 'create' ? "Create" : "Edit"}
                    </Typography>
                    <Box item xs={16} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                        <CustomInput value={nameFood} onChange={(e) => setNameFood(e.target.value)} aria-label="Demo input" placeholder="Name" />
                    </Box>
                    <Box item xs={16} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} marginTop='20px' >
                        <CustomInput value={linkPicture} onChange={(e) => setLinkPicture(e.target.value)} aria-label="Demo input" placeholder="Link Picture" />
                    </Box>
                    <ImageUpload cardName="Input Image" setLinkPicture={(value) => setLinkPicture(value)}  />
                    <Box item xs={16} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} marginTop='30px' >
                        <Button style={{ marginRight: '20px',backgroundColor: 'green' ,color: 'white' }} disabled={nameFood.length === 0 || linkPicture.length === 0 ? true : false} onClick={() => mode === "create" ? handleAddFood() : handleConfirmEdit(indexPick)} variant="contained" color="error" >{mode === "create" ? 'เพิ่ม' : 'แก้ไข'}</Button>
                        <Button style={{ marginLeft: '20px',backgroundColor: 'red',color: 'white' }} onClick={handleClose} variant="contained" color="success" >ยกเลิก</Button>
                    </Box>
                </Box>
            </Modal>
        </Grid>
    )
}
